#include <iostream>
#include <cassert>

using namespace std;

class validate
{
   public:
      validate();
      bool dividebyzero();
      bool notgreaterten();
   private:
      double value;
};

validate::validate()
{
   cin >> value;
}

bool validate::dividebyzero()
{
   if(value == 0)
      return false;
   else
      return true;
}

bool validate::notgreaterten()
{
   if(value > 10)
      return false;
   else
      return true;
}

int main()
{

   cout << "Please enter a number" <<endl;
   validate number;
   
   assert(number.dividebyzero() == true );
   assert(number.notgreaterten() == true );
   
   cout << "SUCCESS "<<endl;

}
